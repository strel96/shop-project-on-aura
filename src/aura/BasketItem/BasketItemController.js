({
    remove : function(component, event, helper){
        helper.removeFromBasket(component);
    },

    changeQuantity : function(component, event, helper){
        helper.changeBasketQuantity(component, event);
    }
});