({
    removeFromBasket : function(component){
        let action = component.get('c.removeFromBasket');

        action.setParams({
            basket : component.get('v.basket')
        });

        action.setCallback(this, function(res){
           let state = res.getState();
           if(state === 'SUCCESS'){
               let removeFromBasketEvent = $A.get('e.c:removeFromBasket');
               removeFromBasketEvent.fire();
               component.set('v.basket', null);
           }
        });

        $A.enqueueAction(action);
    },

    changeBasketQuantity : function(component, event){
        let inputValue = document.getElementById('itemQuantity').value;
        let action = component.get('c.changeBasketQuantity');
        
        action.setParams({
            basket : component.get('v.basket'),
            counter : parseInt(inputValue)
        });

        $A.enqueueAction(action);
    }
});