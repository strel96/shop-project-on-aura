({
    renderBasket : function(component){
        let action = component.get('c.getBaskets');

        action.setCallback(this, function(res){
            let state = res.getState();
            if(state === 'SUCCESS'){
                component.set('v.baskets', res.getReturnValue());
            }
        });

        $A.enqueueAction(action);
    }
});