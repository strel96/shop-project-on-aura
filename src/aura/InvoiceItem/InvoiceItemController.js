({
    changeQuantity : function(component, event, helper) {
        helper.changeBasketQuantity(component, event);
    },
    
    remove : function(component, event, helper){
        helper.removeFromBasket(component);
    }
});