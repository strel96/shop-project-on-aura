({
     removeFromBasket : function(component){
        let action = component.get('c.removeFromInvoice');

        action.setParams({
            invoice : component.get('v.invoice')
        });

        action.setCallback(this, function(res){
           let state = res.getState();
           if(state === 'SUCCESS'){
               let removeFromBasketEvent = $A.get('e.c:removeFromBasket');
               removeFromBasketEvent.fire();
           }
        });

        $A.enqueueAction(action);
    },
    
    changeBasketQuantity : function(component, event){ 
        let action = component.get('c.changeInvoiceQuantity');
       
		let inputValue = event.getSource().get('v.value');
        let maxMerchValue = event.getSource().get('v.max');
        
        if(inputValue < 1 || maxMerchValue < inputValue){
            return;
        }
        
        action.setParams({
            invoice : component.get('v.invoice'),
            counter : parseInt(inputValue)
        });
        
        action.setCallback(this, function(res){
            let state = res.getState();
            if(state === 'SUCCESS'){
                let addEvent = $A.get('e.c:removeFromBasket');
                addEvent.fire();
            }
        }); 

        $A.enqueueAction(action);
    }
});