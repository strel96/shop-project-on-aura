({
    doInit : function(component, event, helper){
        helper.renderBasket(component);
    },

    removeAll : function(component, event, helper){
        helper.remove(component);
    },
    
    buyItems : function(component, event, helper){
        helper.buy(component);
    }

});