({
    renderBasket : function(component){
        let action = component.get('c.getInvoiceItems');

        action.setCallback(this, function(res){
            let state = res.getState();
            if(state === 'SUCCESS'){
                component.set('v.invoices', res.getReturnValue());
            }
        });

        $A.enqueueAction(action);
    },
    
    remove : function(component){
        let action = component.get('c.deleteAllInvoice');
        
        action.setParams({
            invoices : component.get('v.invoices')
        });
        
        action.setCallback(this, function(res){
            let state = res.getState();
            if(state === 'SUCCESS'){
                let addEvent = $A.get('e.c:removeFromBasket');
                addEvent.fire();
            }
        });
        
        $A.enqueueAction(action);
    },
    
    buy : function(component){
        let action = component.get('c.buyInvoiceItems');
        
        action.setParams({
            invoices : component.get('v.invoices')
        });
        
        action.setCallback(this, function(res){
            let state = res.getState();
            if(state === 'SUCCESS'){
                let addEvent = $A.get('e.c:removeFromBasket');
                addEvent.fire();
            }
        }); 
        
        $A.enqueueAction(action);
    }
});