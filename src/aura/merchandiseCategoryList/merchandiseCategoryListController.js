({
    doInit : function(component, event, helper) {
        helper.getCategories(component);
    },
    navigateToRecord : function(component, event) {
        let id = event.target.getAttribute("data-index");
        let item = component.get("v.categories")[id];
        const navEvent = $A.get("e.force:navigateToSObject");
        if(navEvent) {
            navEvent.setParams({
                recordId : item.Id,
                slideDevName: "detail"
            });
            navEvent.fire();
        }
    }
});