({
    getCategories : function(component) {
        let action = component.get("c.getCategories");

        action.setCallback(this, function(res) {
            component.set("v.categories", res.getReturnValue());
        });

        $A.enqueueAction(action);
    }
});