({
    addToBasket : function(component, event) {
        let button = event.getSource();
        button.set("v.disabled", true);

        let action = component.get("c.addToInvoice");

        action.setParams({
            merchandise : component.get("v.merchandise")
        });

        action.setCallback(this, function(res) {
            let state = res.getState();
            if (state === "SUCCESS") {
                let addEvent = $A.get("e.c:addToBasket");
                addEvent.fire();
                button.set("v.disabled", false);
            }
        });

        $A.enqueueAction(action);
    }
});