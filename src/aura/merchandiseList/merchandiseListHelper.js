({
    getId : function(component) {
        let action = component.get("c.getMerchandise");

        let href = window.location.href.split('/');
        let id = href[ href.length - 2 ];

        action.setParams({
            id
        });

        action.setCallback(this, function(res) {
            let state = res.getState();
            if(state === "SUCCESS"){
                component.set("v.merchandises", res.getReturnValue());
            }
        });

        $A.enqueueAction(action);
    }
});