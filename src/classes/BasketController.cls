public with sharing class BasketController {

    @AuraEnabled
    public static List<Invoice_Item__c> getInvoiceItems(){
        String userId = UserInfo.getUserId();

        return [
                SELECT Id, Quantity__c, Merchandise__c, Merchandise__r.Name, Merchandise__r.Quantity__c, Total__c, Basket__c
                FROM Invoice_Item__c
                WHERE UserId__c = :userId
            		AND Basket__r.Status__c = 'Pending'
        ];
    }

    @AuraEnabled
    public static void removeFromInvoice(Invoice_Item__c invoice){

        delete invoice;
    }
    
    @AuraEnabled
    public static void deleteAllInvoice(List<Invoice_Item__c> invoices){

        delete invoices;
    }
    
    @AuraEnabled
    public static void buyInvoiceItems(List<Invoice_Item__c> invoices){
        Set<Id> merchandisesId = new Set<Id>();
        
        for(Invoice_Item__c invoice : invoices){
            merchandisesId.add(invoice.Merchandise__c);
        }
        
      	List<Merchandise__c> merchandises = [SELECT Quantity__c FROM Merchandise__c WHERE Id IN :merchandisesId];
        
        for(Invoice_Item__c invoice : invoices){
            for(Merchandise__c merchandise : merchandises){
                if(invoice.Merchandise__c == merchandise.Id){
                    merchandise.Quantity__c -= invoice.Quantity__c;
                    
                    update merchandise;
                }
            }
        }
        
        Basket__c basket = [SELECT Id FROM Basket__c WHERE Id = :invoices[0].Basket__c];
        basket.Status__c = 'Open';
        
        update basket;
        
    }

    @AuraEnabled
    public static void changeInvoiceQuantity(Invoice_Item__c invoice, Integer counter){
      
        invoice.Quantity__c = counter;
        
        update invoice;
         
    }
}