public with sharing class MerchandiseListController {

    @AuraEnabled(Cacheable = true)
    public static List<Merchandise__c> getMerchandise(String id){
        return [
                SELECT Id, Name, Quantity__c, Price__c, Image__c
                FROM Merchandise__c
                WHERE Merchandise_Category__c = :id
        ];
    }
}