public class MerchandiseTriggerHandler {

    public void addNewItem(List<Merchandise__c> merchandises){

        for(Merchandise__c merchandise : merchandises) {
            if(merchandise.Quantity__c < 3) {
                merchandise.Quantity__c += 5;
            }
        }
    }
}